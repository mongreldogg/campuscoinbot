Okay, great! Lets get started! 
You are now eligible to receive  {coins} coins for a few tasks. Use the tabs below to select your tasks.
 {username_set}
 
[group_join]
Join Telegram group (25 coins)
[/group_join]
[twitter_join]
Join Twitter (25 coins)
[/twitter_join]
[instagram_join]
Join Instagram (25 coins)
[/instagram_join]
[retweet]
Tweet a promo (50 coins)
[/retweet]
[set_wallet]
Set CMPCO wallet address
[/set_wallet]
[refer]
Refer a friend
[/refer]