<?php

namespace Bundle;

use Core\Event;

class BotUser extends DBObject {

	public function setWaitHandle($handle){
		$this->setField('wait_handle', $handle);
	}
	
	public function setHandleData($data){
		$this->setField('handle_data', $data);
	}
	
	public function getWaitHandle(&$data = null){
		$handle = $this->getField('wait_handle');
		$data = $this->getField('handle_data');
		if($handle){
			$this->setField('wait_handle', null);
			$this->setField('handle_data', null);
			$this->Save();
		}
		return $handle;
	}
	
	private static $fields = [
		'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
		'wait_handle' => TYPE_DB_TEXT | TYPE_DB_NULLABLE,
		'handle_data' => TYPE_DB_TEXT | TYPE_DB_NULLABLE
	];
	
	public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'bot_users', $obj);
	}
	
	public static function DefineField($fieldName, $fieldDataType){
		self::$fields[$fieldName] = $fieldDataType;
	}
	
	public static function Select($rules, $count = null, $start = 0){
		return parent::__select($rules, 'bot_users', BotUser::class, $count, $start);
	}
	
	public static function Delete($rules){
		parent::__delete($rules, 'bot_users');
	}
	
	public static function InitTable(){
		parent::__init('bot_users', self::$fields);
	}
	
}

Event::add('onCache', function(){
	BotUser::InitTable();
});