<?php

namespace Bundle;

use Core\IRoute;
use Core\Route;

use Bundle\BotUser;

//TODO: implement request & response associations to store in AIOBot extensions data (for AIO output)
//TODO: implement templates for output messages
//TODO: add response fetching and creation to appropriate interfaces

define('ROUTE_JOIN', SECURE_ROUTE.'::join');
define('ROUTE_LEAVE', SECURE_ROUTE.'::leave');

class AIOBot implements IRoute {
	
	protected static $routes = [];
	protected static $extensions = [];
	protected static $user = null;
	
	public function __construct()
	{
	}
	
	public static function own(){
		return self::$routes;
	}
	
	protected static function generateChildRoutes(BotExtension $extension, $initialPath){
		$extensionType = $extension->getType();
		if(isset(self::$extensions[$extensionType])) {
			$routes = [];
			$routes[$initialPath] = function($command) use($extension) {
				$extension::call($command);
			};
			return $routes;
		} else return [];
	}
	
	public static function call($pattern){
		if(isset(self::$routes[$pattern]))
			self::$routes[$pattern]();
		else{
			foreach(self::$routes as $_pattern=>$callback){
				if(preg_match('/^'.str_replace('/', '\\/', $_pattern).'$/', $pattern)){
					self::$routes[$_pattern]();
				}
			}
		}
	}
	
	public static function on($pattern, $callback){
		foreach(self::$extensions as $name=>$ext){
			self::$routes[$name.'/'.$pattern] = $callback;
		}
		self::$routes[$pattern] = $callback;
	}
	
	public static function RegisterExtension(BotExtension $extension, $initialPath){
		$type = $extension->getType();
		self::$extensions[$type] = $extension;
		Route::delegate( $initialPath, get_class($extension));
	}
	
	public static function getCurrentUser($column = null, $value = null){
		if(!$column && !$value){
			return self::$user;
		}
		BotUser::InitDefaultConnection();
		$user = BotUser::Select([
			$column => $value
		], 1);
		if(!$user) {
			$user = new BotUser([
				$column => $value
			]);
			$user->Save();
		}
		self::$user = $user;
		return $user;
	}
	
	protected static $handleData = null;
	
	public static function getHandleData(){
		return self::$handleData;
	}
	
	public static function WaitFor($route, $store = null){
		$user = self::$user;
		if(!$user) throw new \Exception('Waiting handler initialized without a target user');
		else {
			$user->setWaitHandle($route);
			if($store) $user->setHandleData($store);
			$user->Save();
		}
	}
	
}

interface BotExtension {
	
	public function __construct();
	public static function getUserId();
	public static function Respond(BotResponse $response, $userId = null);
	public static function getType();
	
}

interface BotResponse {
	
	public function __construct($responseData);
	public function getData();
	public function Send($userID = null);
	
}

interface UserMessage {
	
	public function __construct($messageData);
	
}