<?php

namespace Bundle;

class Airdrop extends DBObject implements DBTableObject {

    public function getId(){
        return $this->getField('id');
    }

    public function getUserId(){
        return $this->getField('userid');
    }

    public function setUserId($userId){
        $this->setField('userid'. $userId);
    }

    public function getUserName(){
        return $this->getField('username');
    }

    public function setUserName($username){
        $this->setField('username', $username);
    }

    public function getEmail($email){
        return $this->getField('email');
    }

    public function setEmail($email){
        $this->setField('email', $email);
    }

    public function getTwitterHandle(){
        return $this->getField('twitter');
    }

    public function setTwitterHandle($handle){
        $this->setField('twitter', $handle);
    }

    public function getRetweeted(){
        return $this->getField('retweet');
    }

    public function setRetweeted($retweeted){
        $this->setField('retweet', (int) $retweeted);
    }

    public function getInstagram(){
        return $this->getField('instagram');
    }

    public function setInstagram($instagram){
        $this->setField('instagram', $instagram);
    }

    public function getReferrer(){
        return $this->getField('referrer');
    }

    public function setReferrer($referrer){
        $this->setField('referrer', $referrer);
    }

    public function getReferrals(){
        return $this->getField('referrals');
    }

    public function setReferrals($referrals){
        $this->setField('referrals', $referrals);
    }

    public function getTokens(){
        return $this->getField('tokens');
    }

    public function setTokens($tokens){
        $this->setField('tokens', $tokens);
    }

    public function getWallet(){
        return $this->getField('wallet');
    }

    public function setWallet($wallet){
        $this->setField('wallet', $wallet);
    }

    public function getGroupJoin(){
        return $this->getField('group_join');
    }

    public function setGroupJoin($flag){
        $this->setField('group_join', (int)$flag);
    }

    public function Reward($tokens){
        $this->setField('tokens', (int)$this->getField('tokens') + $tokens);
    }

    private static $fields = [
        'id' => TYPE_DB_NUMERIC | TYPE_DB_PRIMARY,
        'userid' => TYPE_DB_NUMERIC, 
        'username' => TYPE_DB_TEXT,
        'email' => TYPE_DB_TEXT,
        'twitter' => TYPE_DB_TEXT,
        'retweet' => TYPE_DB_NUMERIC,
        'instagram' => TYPE_DB_TEXT,
        'referrer' => TYPE_DB_NUMERIC,
        'referrals' => TYPE_DB_NUMERIC,
        'tokens' => TYPE_DB_NUMERIC,
        'wallet' => TYPE_DB_TEXT,
        'group_join' => TYPE_DB_NUMERIC
    ];

    public function __construct($obj = null)
	{
		parent::__construct(self::$fields, 'airdrop', $obj);
	}
	
	public static function Init(){
		parent::__init('airdrop', self::$fields);
	}
	
	public static function Select($rules, $count = null, $start = 0)
	{
		return parent::__select($rules, 'airdrop', Airdrop::class, $count, $start);
	}
	
	public static function Delete($rules)
	{
		parent::__delete($rules, 'airdrop');
	}
	
	public static function Count($rules = [])
	{
		return parent::__selectCount($rules, 'airdrop');
	}

}