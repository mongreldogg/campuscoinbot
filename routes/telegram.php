<?php

use Bundle\BountyCode;
use Bundle\Captcha;
use Bundle\Forehood;
use Core\Template;

use Bundle\Telegram;
use Bundle\TelegramResponse;
use Bundle\Validator;
use Bundle\Airdrop;
use Bundle\GroupJoin;

function mainMenu(Airdrop $airdrop, $template = null){
	if(!$template)
		$template = Template::Get('bot/main');
	$username = Telegram::getUserName();
	$replace = [
		'username_set' => ''
	];
	$buttons = [];
	$buttonHandles = [];
	/*if(strlen($airdrop->getUserName()) == 0 && strlen($username) > 0){
		$airdrop->setUserName($username);
		$airdrop->Reward(REWARD_USERNAME_SET);
		$airdrop->Save();
		$replace['username_set'] = Template::Get('bot/username_set');
	}*/
	if(!GroupJoin::IsGroupJoined(Telegram::getUserId())){
		$button = trim(Template::Tag('group_join', $template));
		$buttons[] = $button;
		$buttonHandles[$button] = 'group_join';
	} elseif(!$airdrop->getGroupJoin()) {
		$airdrop->setGroupJoin(1);
		$airdrop->Reward(REWARD_TELEGRAM_JOIN);
		$airdrop->Save();
		Template::Tag('group_join', $template);
	} else Template::Tag('group_join', $template);
	if(strlen($airdrop->getTwitterHandle()) == 0){
		$button = trim(Template::Tag('twitter_join', $template));
		$buttons[] = $button;
		$buttonHandles[$button] = 'twitter_join';
	} else Template::Tag('twitter_join', $template);
	if(strlen($airdrop->getInstagram()) == 0){
		$button = trim(Template::Tag('instagram_join', $template));
		$buttons[] = $button;
		$buttonHandles[$button] = 'instagram_join';
	} else Template::Tag('instagram_join', $template);
	if($airdrop->getRetweeted() == 0){
		$button = trim(Template::Tag('retweet', $template));
		$buttons[] = $button;
		$buttonHandles[$button] = 'retweet';
	} else Template::Tag('retweet', $template);
	$button = trim(Template::Tag('set_wallet', $template));
	$buttons[] = $button;
	$buttonHandles[$button] = 'set_wallet';
	$button = trim(Template::Tag('refer', $template));
	$buttons[] = $button;
	$buttonHandles[$button] = 'refer';
	$bidx = 0; $_buttons = [];
	foreach($buttons as $btn){
		$bidx++;
		switch($bidx){
			case 1:
				$_buttons[] = [$btn];
				break;
			case 2:
				$_buttons[sizeof($_buttons) - 1][1] = $btn;
				break;
			case 3:
				$_buttons[sizeof($_buttons) - 1][2] = $btn;
				break;
			default:
				$bidx = 1;
				$_buttons[] = [$btn];
				break;
		}
	}
	$replace['coins'] = $airdrop->getTokens();
	TelegramResponse::Buttons($_buttons);
	$airdrop = array_merge($airdrop->Serialize(), ['buttons'=>$buttonHandles]);
	Telegram::WaitFor(SECURE_ROUTE.'menu::pick', json_encode($airdrop, true));
	TelegramResponse::Text(Template::Replace($replace, trim($template)))->Send();
}

global $handleProc;
$handleProc = [

	'group_join' => function(Airdrop $airdrop){
		Telegram::WaitFor('back');
		$template = Template::Get('bot/group_join');
		$template = Template::Replace([
			'group_name' => TELEGRAM_LINK
		], $template);
		$back = Template::Tag('back', $template);
		TelegramResponse::Buttons(trim($back));
		TelegramResponse::Text($template)->Send();
	},

	'twitter_join' => function(Airdrop $airdrop){
		Telegram::WaitFor(SECURE_ROUTE.'twitter::handle', json_encode($airdrop->Serialize(), true));
		$template = Template::Get('bot/twitter_join');
		$back = Template::Tag('back', $template);
		TelegramResponse::Buttons(trim($back));
		TelegramResponse::Text(Template::Replace([
			'twitter_link' => TWITTER_LINK
		], $template))->Send();
	},

	'instagram_join' => function(Airdrop $airdrop){
		Telegram::WaitFor(SECURE_ROUTE.'instagram::handle', json_encode($airdrop->Serialize(), true));
		$template = Template::Get('bot/instagram_join');
		$back = Template::Tag('back', $template);
		TelegramResponse::Buttons(trim($back));
		TelegramResponse::Text(Template::Replace([
			'instagram_link' => INSTAGRAM_LINK
		], $template))->Send();
	},

	'refer' => function(Airdrop $airdrop){
		Telegram::WaitFor('back');
		$template = Template::Get('bot/refer');
		$template = Template::Replace([
			'coins' => REWARD_REFERRER,
			'bot_name' => TELEGRAM_BOT_NAME,
			'referral_code' => Telegram::getUserId()
		], $template);
		$back = Template::Tag('back', $template);
		TelegramResponse::Buttons($back);
		TelegramResponse::Text($template)->Send();
	},

	'retweet' => function(Airdrop $airdrop){
		Telegram::WaitFor(SECURE_ROUTE.'retweeted', json_encode($airdrop->Serialize(), true));
		$template = Template::Get('bot/retweet'); //TODO: fix emoticons

		$back = Template::Tag('honesty', $template);
		TelegramResponse::Buttons(trim($back));
		TelegramResponse::Text(
			Template::Replace([
				'coins' => REWARD_RETWEET,
				'bot_name' => TELEGRAM_BOT_NAME,
				'referral_code' => Telegram::getUserId()
			],$template)
		)->Send();
	},

	'set_wallet' => function(Airdrop $airdrop){
		if(!($airdrop instanceof Airdrop)) TelegramResponse::Text(Template::Get('bot/out_of_order'))->Send();
		Telegram::WaitFor(SECURE_ROUTE.'type::address', json_encode($airdrop->Serialize(), true));
		TelegramResponse::Text(Template::Get('bot/type_address'))->Send();
	}

];

Telegram::on(SECURE_ROUTE.'retweeted', function(){
	$airdrop = json_decode(Telegram::getHandleData(), true);
	if($airdrop['id']) $airdrop = Airdrop::Select(['id' => $airdrop['id']], 1);
	else $airdrop = new Airdrop($airdrop);
	$airdrop->Reward(REWARD_RETWEET);
	$airdrop->setRetweeted(1);
	$airdrop->Save();
	$response = Template::Replace(['coins' => REWARD_RETWEET],Template::Get('bot/retweeted'));
	mainMenu($airdrop, $response.Template::Get('bot/main'));
});

Telegram::on(SECURE_ROUTE.'menu::pick', function(){
	global $handleProc;
	$data = Telegram::getHandleData();
	$command = Telegram::getCommand();
	$airdrop = json_decode($data, true);
	$buttonHandles = $airdrop['buttons'];
	unset($airdrop['buttons']);
	if(isset($airdrop['id'])) $airdrop = Airdrop::Select(['id' => $airdrop['id']], 1);
	else $airdrop = new Airdrop($airdrop);
	if(isset($buttonHandles[$command])){
		$handleProc[$buttonHandles[$command]]($airdrop);
	} else 
		mainMenu($airdrop, Template::Get('bot/unrecognized').Template::Get('bot/main'));
});

Telegram::on(SECURE_ROUTE.'twitter::handle', function(){
	$airdrop = json_decode(Telegram::getHandleData(), true);
	if($airdrop['id']) $airdrop = Airdrop::Select(['id' => $airdrop['id']], 1);
	else $airdrop = new Airdrop($airdrop);
	$handle = Telegram::getCommand();
	$airdrop->setTwitterHandle($handle);
	$airdrop->Reward(REWARD_TWITTER_JOIN);
	$airdrop->Save();
	$success = Template::Replace([
			'coins' => REWARD_TWITTER_JOIN
	], Template::Get('bot/twitter_success'));

	mainMenu($airdrop, $success.Template::Get('bot/main'));
});

Telegram::on(SECURE_ROUTE.'instagram::handle', function(){
	$airdrop = json_decode(Telegram::getHandleData(), true);
	if($airdrop['id']) $airdrop = Airdrop::Select(['id' => $airdrop['id']], 1);
	else $airdrop = new Airdrop($airdrop);
	$handle = Telegram::getCommand();
	$airdrop->setInstagram($handle);
	$airdrop->Reward(REWARD_INSTAGRAM_JOIN);
	$airdrop->Save();
	$success = Template::Replace([
			'coins' => REWARD_INSTAGRAM_JOIN
	], Template::Get('bot/instagram_success'));

	mainMenu($airdrop, $success.Template::Get('bot/main'));
});

Telegram::on('back', function(){
	$id = Telegram::getUserId();
	$airdrop = Airdrop::select(['userid' => $id], 1);
	mainMenu($airdrop);
});

Telegram::on(ROUTE_JOIN, function(){
	$id = Telegram::getUserId();
	$join = GroupJoin::Select(['telegram_user_id' => $id], 1);
	if($join instanceof GroupJoin)
		$join->setJoined(1);
	else $join = new GroupJoin([
		'telegram_user_id' => $id,
		'joined' => 1
	]);
	$join->Save();
});

Telegram::on(ROUTE_LEAVE, function(){
	$id = Telegram::getUserId();
	$join = GroupJoin::Select(['telegram_user_id' => $id], 1);
	if($join instanceof GroupJoin){
		$join->setJoined(0);
	} else {
		$join = new GroupJoin([
			'telegram_user_id' => $id,
			'joined' => 0
		]);
	}
	$join->Save();
	$airdrop = Airdrop::Select(['userid' => $id], 1);
	if($airdrop instanceof Airdrop){
		if($airdrop->getGroupJoin() == 1){
			$airdrop->Reward(0 - REWARD_TELEGRAM_JOIN);
			$airdrop->setGroupJoin(0);
			$airdrop->Save();
		}
	}
	Telegram::DeleteMessage();
});

Telegram::on('/start.*', function(){
	if(Telegram::getUserId() != Telegram::getChatId()) exit;
	$command = Telegram::getCommand();
	$referrer = 0;
	if($command != '/start'){
		$referrer = (int)trim(str_replace('/start ', '', $command));
	}
	$userId = Telegram::getUserId();
	$airdrop = Airdrop::Select([
		'userid' => $userId
	], 1);
	if(!($airdrop instanceof Airdrop)) {
		$ref = Airdrop::Select([
			'userid' => $referrer
		], 1);
		if($ref instanceof Airdrop){
			$ref->setReferrals($ref->getReferrals() + 1);
			sendCaptcha('', ['referrer'=>$ref->Serialize()]);
		} else 
			sendCaptcha();
	} else mainMenu($airdrop);
	exit;
	
});

Telegram::on(SECURE_ROUTE.'type::email', function(){
	$data = Telegram::getHandleData();
	$airdrop = new Airdrop(json_decode($data, true));
	$email = Telegram::getCommand();
	if(!Validator::Email($email)){
		Telegram::WaitFor(SECURE_ROUTE.'type::email', $data);
		TelegramResponse::Text(Template::Get('bot/wrong_email'))->Send();
	} else {
		$airdrop->setEmail($email);
		if(preg_match('/\.edu$/', $email)) $airdrop->Reward(REWARD_EDU_EMAIL);
		$airdrop->Save();
		mainMenu($airdrop);
	}
});

Telegram::on(SECURE_ROUTE.'type::address', function(){
	$airdrop = json_decode(Telegram::getHandleData(), true);
	if($airdrop['id']) $airdrop = Airdrop::Select(['id' => $airdrop['id']], 1);
	else $airdrop = new Airdrop($airdrop);
	$address = Telegram::getCommand();
	if(Validator::CMPCO($address)){
		if(!(strlen($airdrop->getWallet()) > 0) && $airdrop->getReferrer() > 0){
			$referrer = Airdrop::Select(['userid'=>$airdrop->getReferrer()],1);
			$referrer->Reward(REWARD_REFERRER); //TODO: migrate to start
			$referrer->Save();
		}
		$airdrop->setWallet($address);
		$airdrop->Save();
		mainMenu($airdrop, Template::Get('bot/type_address_success').
			Template::Get('bot/main'));
	} else {
		mainMenu($airdrop, Template::Get('bot/wrong_address').
			Template::Get('bot/main'));
	}
});

Telegram::on(SECURE_ROUTE.'type::captcha', function(){
	$answer = Telegram::getCommand();
	$data = json_decode(Telegram::getHandleData(), true);
	@Captcha::Delete(['id' => $data['captcha_id']]);
	if(strtolower($answer) == strtolower($data['answer'])){
		$userId = Telegram::getUserId();
		$airdrop = new Airdrop([
			'userid' => $userId,
			'username' => Telegram::getUserName()
		]);
		if(@$referrer = $data['referrer']['id']){
			$ref = Airdrop::Select([
				'id' => $referrer
			], 1);
			if($ref instanceof Airdrop){
				$airdrop->setReferrer($ref->getUserId());
				$airdrop->Save();
				$ref->setReferrals($ref->getReferrals() + 1);
				$ref->Save();
			}
		}
		Telegram::WaitFor(SECURE_ROUTE.'type::email', json_encode($airdrop->Serialize(), true));
		TelegramResponse::Text(Template::Get('bot/type_email'))->Send();
	} else {
		sendCaptcha(Template::Get('bot/wrong_captcha'), $data);
	}
});

function sendCaptcha($template = null, $handleData = []){
	if(!$template) $template = '';
	$captcha = Captcha::Generate();
	if(!is_array($handleData)) $handleData = [];
	foreach(['captcha_id', 'question', 'answer'] as $param){
		if(isset($handleData[$param])) unset($handleData[$param]);
	}
	if($captcha instanceof Captcha){
		$response = $template.Template::Get('bot/type_captcha_picture');
		Telegram::WaitFor(SECURE_ROUTE.'type::captcha', json_encode(array_merge([
			'captcha_id' => $captcha->getId(),
			'question' => $captcha->getQuestion(),
			'answer' => $captcha->getAnswer()
		], $handleData), true));
		TelegramResponse::Image('https://'.HOSTNAME.ROOT_DIR.'captcha/'.$captcha->getQuestion(), $response)
			->Send();
	}
}

Telegram::on('.*', function(){
	if(Telegram::getUserId() != Telegram::getChatId()) exit;
	TelegramResponse::Text(Template::Get('bot/out_of_order'))->Send();
});
