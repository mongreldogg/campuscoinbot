<?php

use Bundle\Captcha;
use Core\Request;
use Core\Route;
use Core\Response;
use Core\Event;
use Core\Template;

Route::on('captcha/.*', function(){

	$id = str_replace(ROOT_DIR.'captcha/', '', Request::Path());
	
	header ("Content-type: image/jpeg");
	
	$width = 150;
	$height = 50;
	$xpos = 25;
	$ypos = 18;
	
	$im = imagecreate ($width, $height) or die ("You must activate GD library on your web server");
	
	for ($i = 0; $i < 30; $i++)
	{
		$color = imagecolorallocate ($im, rand(0,128), rand(0,128), rand(0,128));
		imageline($im, rand(0,$width), rand(0,$height), rand(0,$width), rand(0,$height), $color);
		imageellipse($im, rand(0,$width), rand(0,$height), rand(0,$width), rand(0,$height), $color);
	}
	
	$captcha = Captcha::Select(['question' => $id], 1);
	if($captcha instanceof Captcha)
	$code = $captcha->getAnswer();
	
	$key = [];
	for($i=0;$i<strlen($code);$i++) $key[$i] = substr($code, $i, 1);
	
	foreach ($key as $value)
	{
		$color = imagecolorallocate ($im, rand(128,255), rand(128,255), rand(128,255));
		imagestring ($im, 16, $xpos, $ypos, $value, $color);
		$xpos += 22;
	}
	
	imagejpeg($im, null, $width);
	imagedestroy($im);
	
	exit;
	

});

function downloadReport(){

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=report.xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	$db = Captcha::initDefaultConnection();
	$mapping = [
		'email',
		'username',
		'twitter',
		'instagram',
		'retweet',
		'wallet',
		'tokens'
	];
	$result = $db->Result($db->Query(
		"SELECT ".implode(',', $mapping)." FROM airdrop ".
		"WHERE wallet > ''"
	), RESULT_TYPE_NUMERIC);
	$response = implode("\t", $mapping)."\r\n";
	foreach($result as $row){
		$response .= implode("\t", $row)."\r\n";
	}
	print $response;

}

Route::on('download', function(){
	if(@$_POST['password'] == DASHBOARD_PASSWORD){
		downloadReport();
	} else {
		Response::HTML(Template::Get('web/auth'));
	}
});

Route::on(NOTFOUND, function(){
	Response::HTML(Template::Get('web/dummy'));
});
