$(document).ready(function(){

    $(document).on('click', '.rewarded', function(){

        var item = $(this);

        $.ajax({
            method: 'POST',
            url: 'dashboard',
            data: {
                ajax: 'setRewarded',
                userId: item.parent('.list-row').attr('data-id')
            }
        }).always(function(response){
            item.parent('.list-row').fadeOut(100);
            item.parent('.list-row').remove();
            if($('.list-row').length == 0) {
                console.log('refreshy');
                document.location.href = 'dashboard';
            }
        });

    });

});