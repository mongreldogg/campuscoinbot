<?php

//Use for URL and route definition itself
define('NOTFOUND', '404');
//Main URL directory assignment
define('ROOT_DIR', '/campuscoinbot/');

//Disables caching when enabled
define('DEBUG_MODE', 0);
//Route prefix for secure routes for inner calls
define('SECURE_ROUTE', '__');

define('HOSTNAME', 'tbottest.tk');
define('FOREHOOD_DEAFULT_TIMEOUT', 10000); //ms

