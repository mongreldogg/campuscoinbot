<?php

define('CAPTCHA_ATTEMPTS', 3);
define('CAPTCHA_MD5_SALT', 'saltyson');

define('PROJECT_NAME', 'Campus Coin Airdrop');
define('DASHBOARD_PASSWORD', 'campusleader');
define('DASHBOARD_ITEMS_PER_PAGE', 100);

define('REWARD_EDU_EMAIL', 10);
define('REWARD_REFERRER', 10);
define('REWARD_USERNAME_SET', 25);

define('REWARD_TELEGRAM_JOIN', 25);
define('REWARD_TWITTER_JOIN', 25);
define('REWARD_INSTAGRAM_JOIN', 25);
define('REWARD_RETWEET', 50);
